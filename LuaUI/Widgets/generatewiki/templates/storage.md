## Introduction

This is a storage, i.e. a static building meant to store ammo. You really don't want to run out of ammo, so ensure to have several ammo storages. On the other hand, storages are quite expensive structures, so don't spam too much storages, or they'll drain your precious command points, and build them as spaced as possible to avoid a chain destruction in a single strike. You can learn more about Ammo storage and supply [here](ammo).

Of course, this units should be placed as far away as possible of enemy fire.

{comments}

## Structural details

| Parameter                  | Value       | Parameter                | Value           |
|----------------------------|-------------|--------------------------|-----------------|
| ![Cost][1] Cost            | {buildCost} | ![Time][2] Build time    | {buildTime}     |
| ![Health][3] Health points | {maxDamage} | ![Ammo][4] Ammo capacity | {energyStorage} |


![Category][104] Targeted as: {categories}

![Armor][105] Damaged as: {armorType}


[1]: /uploads/ec651a1312826e75c31e416dad059540/hammer_icon.svg
[2]: /uploads/6b5dd9ae4065b8de00d2e1c15aa774d6/clock_icon.svg
[3]: /uploads/129159344ebabef123d1fcb5db9823a2/heart_icon.svg
[4]: /uploads/36e910c23dd318832e2ba5e72c5738dc/ammo_icon.svg
[104]: /uploads/c9800e87cd30bc07a5fbf59d5ff2ae73/accuracy_icon.svg
[105]: /uploads/5e43d946a83a1c4661c4da46dba6c60d/explosion_icon.svg

