### {name}

{comments}

| Parameter | Minimum | Maximum | Speed |
|-----------|---------|---------|-------|
| ![Heading][101] Heading | {minHeading} º | {maxHeading} º | {speedHeading} º/s |
| ![Pitch][102] Pitch | {minPitch} º | {maxPitch} º | {speedPitch} º/s |

| Parameter | Value | Parameter | Value |
|-----------|-------|-----------|-------|
| ![Range][103] Range | {range} m | ![Explosion][105] Effect radius | {damageArea} m |
| ![Accuracy][104] Inaccuracy | {accuracy} | ![Accuracy][104] Moving inaccuracy | {movingAccuracy} |
| ![Penetration][106] Penetration (100m) | {pen100} mm | ![Penetration][106] Penetration (1000m) | {pen1000} mm |
| ![Reload][107] Fire rate | {fireRate} bullets/s | ![AmmoCost][108] Ammo cost | {ammoCost} units/bullet | 

![Targets][104] Targets: {targets}

![Damage][105] Damage: {damages}

[101]: /uploads/f34b312f778ec209c9dc8a0908e75202/gunheading_icon.svg
[102]: /uploads/153c9619ab329f1570600fbc417cd120/gunpitch_icon.svg
[103]: /uploads/c3e0251c2814e7b78dc31210ad7ea55b/range_icon.svg
[104]: /uploads/c9800e87cd30bc07a5fbf59d5ff2ae73/accuracy_icon.svg
[105]: /uploads/5e43d946a83a1c4661c4da46dba6c60d/explosion_icon.svg
[106]: /uploads/7c5b9b7ffed7e56a91d4746c5f33ad00/penetration.svg
[107]: /uploads/cc6b71fe47afdf8091bddb96958eb167/reload_icon.svg
[108]: /uploads/36e910c23dd318832e2ba5e72c5738dc/ammo_icon.svg
