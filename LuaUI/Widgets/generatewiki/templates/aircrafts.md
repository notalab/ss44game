## Introduction

{subclass_comments}

{comments}

## Structural details

| Parameter | Value | Parameter | Value |
|-----------|-------|-----------|-------|
| ![Cost][1] Cost | {buildCost} | ![Health][2] Health points | {maxDamage} |


![Category][104] Targeted as: {categories}

![Armor][105] Damaged as: {armorType}

{maxammo}

## Line of sight

| Parameter | Value | Parameter | Value | Parameter | Value |
|-----------|-------|-----------|-------|-----------|-------|
| ![LOS][4] Sight range | {sight} | ![AirLOS][5] Air detection | {airLOS} | ![SeismicLOS][6] Noise detection | {noiseLOS} |

## Motion

| Parameter | Value | Parameter | Value | Parameter | Value |
|-----------|-------|-----------|-------|-----------|-------|
| ![Speed][7] Max speed | {maxspeed} km/h | ![Turn][8] Turn rate | {turn} º/s | ![Fuel][9] Max fuel | {fuel} s |


[1]: /uploads/ec651a1312826e75c31e416dad059540/hammer_icon.svg
[2]: /uploads/129159344ebabef123d1fcb5db9823a2/heart_icon.svg
[4]: /uploads/bb4a87c1fbd3e710e7deca7d9e688d2d/binocs_icon.svg
[5]: /uploads/033c3b5aa6f1593dbe2d0801ff3a0bc2/airplane_icon.svg
[6]: /uploads/ac90981358eb62b4a825102de17d67e1/tank_icon.svg
[7]: /uploads/acc7ac30108162f06483d0faababe4bd/run_icon.svg
[8]: /uploads/dda0f867d1d99d161acc0208e89775b0/turn_icon.svg
[9]: /uploads/c9f4640d2c4cf622cd1f0736297eaaf7/fuel_icon.svg
[11]: /uploads/36e910c23dd318832e2ba5e72c5738dc/ammo_icon.svg
[104]: /uploads/c9800e87cd30bc07a5fbf59d5ff2ae73/accuracy_icon.svg
[105]: /uploads/5e43d946a83a1c4661c4da46dba6c60d/explosion_icon.svg
[106]: /uploads/7c5b9b7ffed7e56a91d4746c5f33ad00/penetration.svg

