local modinfo = {
    name = 'Steam Spring 1944',
    shortName = 'ss44',
    version = 'alpha-$VERSION',
    game = 'Steam Spring 1944',
    shortGame = 'ss44',
    mutator = 'notAlab distribution',
    description = 'Epic World War II RTS',
    url = 'http://notalab.com/products/ss44/',
}

return modinfo
